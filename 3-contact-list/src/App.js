import React, { useState, useEffect } from 'react';
import peopleApi from './api/people';
import ContactInfoList from './components/ContactInfoList';
import PersonList from './components/PersonList';

const App = () => {
	const [people, setPeople] = useState([]);
	const [person, setPerson] = useState(null);
	const [contactInfos, setContactInfos] = useState([]);

	const onPersonClick = async (person) => {
		setPerson(person);

		const result = await peopleApi.get(`${person.id}/info`);
		setContactInfos(result.data.data);
	};

	const onContactInfoDeleteClick = async (person, contactInfo) => {
		if (window.confirm('Are you sure about deleting the contact info?')) {
			await peopleApi.delete(`${person.id}/info/${contactInfo.id}`);

			const result = await peopleApi.get(`${person.id}/info`);
			setContactInfos(result.data.data);
		}
	};

	useEffect(() => {
		peopleApi.get().then((result) => {
			setPeople(result.data.data);
		});
	}, []);

	return (
		<div className="app">
			<header className="app-header">
				<div className="container">
					<h1 className="title is-1">Bravi - Contact List</h1>
				</div>
			</header>
			<main className="app-main">
				<div className="app-main__people container">
					<PersonList
						people={people}
						onPersonClick={onPersonClick}
					></PersonList>
				</div>
				<div className="container">
					{person && contactInfos && (
						<ContactInfoList
							person={person}
							contactInfos={contactInfos}
							onDeleteClick={onContactInfoDeleteClick}
						></ContactInfoList>
					)}
				</div>
			</main>
		</div>
	);
};

export default App;
