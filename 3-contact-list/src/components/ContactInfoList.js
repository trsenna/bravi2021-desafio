import React from 'react';

const ContactInfoList = ({ person, contactInfos, onDeleteClick }) => {
	const tableRows = contactInfos.map((contactInfo) => {
		return (
			<tr key={contactInfo.id}>
				<td>{contactInfo.id}</td>
				<td>{contactInfo.type}</td>
				<td>{contactInfo.value}</td>
				<td>
					<button onClick={() => onDeleteClick(person, contactInfo)}>
						[delete]
					</button>
				</td>
			</tr>
		);
	});

	return (
		<div className="contact-info-list">
			<h3 className="title is-3">{person.name}</h3>
			<table className="table is-fullwidth">
				<thead>
					<tr>
						<th>#</th>
						<th>Type</th>
						<th>Value</th>
						<th></th>
					</tr>
				</thead>
				<tbody>{tableRows}</tbody>
			</table>
		</div>
	);
};

export default ContactInfoList;
