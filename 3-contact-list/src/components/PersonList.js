import React from 'react';

const PersonList = ({ people, onPersonClick }) => {
	const tableRows = people.map((person) => {
		return (
			<tr key={person.id}>
				<td>{person.id}</td>
				<td>{person.name}</td>
				<td>
					<button onClick={() => onPersonClick(person)}>
						[show contact info]
					</button>
				</td>
			</tr>
		);
	});

	return (
		<div className="person-list">
			<h2 className="title is-2">All Contacts</h2>
			<table className="table is-fullwidth">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>{tableRows}</tbody>
			</table>
		</div>
	);
};

export default PersonList;
