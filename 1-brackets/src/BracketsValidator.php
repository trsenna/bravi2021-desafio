<?php

final class BracketsValidator
{
    const BRACKETS = [
        '(' => ')',
        '[' => ']',
        '{' => '}'
    ];

    public function isValid(string $content): bool
    {
        $valid = true;

        // if length is odd, so no way to be valid
        if (strlen($content) % 2 !== 0) {
            $valid = false;
        }

        $open = [];
        $contentLength = strlen($content);
        for ($i = 0; $i < $contentLength; $i++) {
            // just save all tags that opens
            if (in_array($content[$i], array_keys(self::BRACKETS))) {
                $open[] = $content[$i];
                continue;
            }

            // fail when content starts with closing brackets
            if (empty($open)) {
                $valid = false;
                break;
            }

            // check if closing tag makes a pair with the opening tag
            if ($content[$i] !== self::BRACKETS[$open[sizeof($open) - 1]]) {
                $valid = false;
                break;
            }

            array_pop($open);
        }

        // a last bracket was not properly closed
        if (!empty($open)) {
            $valid = false;
        }

        return $valid;
    }
}
