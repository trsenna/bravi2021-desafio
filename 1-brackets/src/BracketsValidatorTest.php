<?php

use PHPUnit\Framework\TestCase;

final class BracketsValidatorTest extends TestCase
{
    public function testIsValidWhenAllBracketsOpensAndClosesProperlyShouldReturnTrue(): void
    {
        $validator = new BracketsValidator();
        $this->assertTrue($validator->isValid('(){}[]'));
        $this->assertTrue($validator->isValid('[{()}](){}'));
    }

    public function testIsValidWhenBracketsOpensButNotClosesProperlyShouldReturnFalse(): void
    {
        $validator = new BracketsValidator();
        $this->assertFalse($validator->isValid('[]{()'));
        $this->assertFalse($validator->isValid('[{)]'));
    }

    public function testIsValidWhenBracketsOpensButNotClosesProperlyShouldReturnFalse__2(): void
    {
        $validator = new BracketsValidator();
        $this->assertFalse($validator->isValid(')){}[]'));
        $this->assertFalse($validator->isValid('(({})('));
    }
}
