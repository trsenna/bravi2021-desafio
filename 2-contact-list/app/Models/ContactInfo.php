<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    use HasFactory;

    const TYPE_PHONE = 'phone';
    const TYPE_WHATSAPP = 'whatsapp';
    const TYPE_EMAIL = 'e-mail';

    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'type',
        'value',
    ];
}
