<?php

namespace App\Http\Controllers;

use App\Models\ContactInfo;
use App\Models\Person;
use Illuminate\Http\Request;

class ContactInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($personId)
    {
        $person = Person::findOrFail($personId);
        $contactInfos = $person->info;

        return response()->json([
            'status' => 'success',
            'data' => $contactInfos,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($personId, $id)
    {
        $person = Person::findOrFail($personId);
        $contactInfo  = $person->info()->where('id', $id)->first();

        return response()->json([
            'status' => 'success',
            'data' => $contactInfo,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($personId, $id)
    {
        $person = Person::findOrFail($personId);
        $contactInfo  = $person->info()->where('id', $id)->first();

        if ($contactInfo) {
            $contactInfo->delete();
        }

        return response()->json([
            'status' => 'success',
            'data' => $contactInfo,
        ]);
    }
}
