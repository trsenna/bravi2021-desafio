<?php

namespace Database\Factories;

use App\Models\ContactInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContactInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid(),
            'type' => $this->faker->randomElement([ContactInfo::TYPE_PHONE, ContactInfo::TYPE_WHATSAPP]),
            'value' => $this->faker->phoneNumber(),
        ];
    }
}
